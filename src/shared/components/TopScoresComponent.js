import React from "react";
import { View } from "react-native";
import TextComponent from "./TextComponent";
import styles from "./TopScoresComponent.style";

const TopScoresComponent = (props) => {
  const { table, tableIndex } = props;

  return (
    <View style={styles.scoresContainer} key={tableIndex}>
      <View style={styles.textContainer}>
        <TextComponent style="headerText" text={Object.keys(table)[0]} />
        {table[Object.keys(table)[0]].places.map((player, index) => (
          <View key={index}>
            <TextComponent style="placeText" text={index + 1 + " place"} />
            <View style={styles.playerContainer}>
              <TextComponent style="playerText" text={player.name} />
              <TextComponent
                style="playerText"
                text={player.score + " points"}
              />
            </View>
          </View>
        ))}
      </View>
    </View>
  );
};

export default React.memo(TopScoresComponent);

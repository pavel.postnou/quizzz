import React, { useState } from "react";
import { View, TouchableHighlight } from "react-native";
import TextComponent from "./TextComponent";
import styles from "./AnswerComponent.style";

const AnswerComponent = (props) => {
  const {
    question,
    index,
    disabled,
    setDisabled,
    style,
    setStyle,
    setIndex,
    questionIndex,
    rightAnswer,
    setRightAnswer,
  } = props;
  const [right, setRight] = useState(null);

  const checkAnswer = (right, answer) => {
    right === answer
      ? (setRight("buttonRight"), setRightAnswer(rightAnswer + 1))
      : setRight("buttonWrong");
    setStyle("buttonDisabled");
    setDisabled(true);
    let count = questionIndex;
    setTimeout(() => setIndex(++count), 1000);
  };

  if (right != null)
    return (
      <TouchableHighlight
        style={styles.touch}
        disabled={disabled}
        onPress={() =>
          checkAnswer(question?.rightAnswer, question?.answers[index])
        }
      >
        <View style={styles[right]}>
          <TextComponent style="textButton" text={question?.answers[index]} />
        </View>
      </TouchableHighlight>
    );
  else
    return (
      <TouchableHighlight
        style={styles.touch}
        disabled={disabled}
        onPress={() =>
          checkAnswer(question?.rightAnswer, question?.answers[index])
        }
      >
        <View style={styles[style]}>
          <TextComponent style="textButton" text={question?.answers[index]} />
        </View>
      </TouchableHighlight>
    );
};

export default AnswerComponent;

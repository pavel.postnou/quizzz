import React, { useState, useEffect } from "react";
import { View } from "react-native";
import ImageComponent from "./ImageComponent";
import AnswerComponent from "./AnswerComponent";
import TextComponent from "./TextComponent";
import styles from "./QuizScreenComponent.style";

const QuizScreenComponent = (props) => {
  const [disabled, setDisabled] = useState(false);
  const [style, setStyle] = useState("button");

  const { question, questionIndex, setIndex, rightAnswer, setRightAnswer } =
    props;

  useEffect(() => {
    setStyle("button");
    setDisabled(false);
  }, [questionIndex]);

  return (
    <View style={styles.itemContainer} key={questionIndex}>
      <View style={styles.textContainer}>
        <View style={styles.imageContainer}>
          <ImageComponent uri={question.image} />
        </View>
        <TextComponent style="headerText" text={question.question} />
        <View style={styles.buttonsContainer}>
          <View style={styles.columnContainer}>
            <AnswerComponent
              question={question}
              index={0}
              disabled={disabled}
              setDisabled={setDisabled}
              style={style}
              setStyle={setStyle}
              setIndex={setIndex}
              questionIndex={questionIndex}
              rightAnswer={rightAnswer}
              setRightAnswer={setRightAnswer}
            />
            <AnswerComponent
              question={question}
              index={1}
              disabled={disabled}
              setDisabled={setDisabled}
              style={style}
              setStyle={setStyle}
              setIndex={setIndex}
              questionIndex={questionIndex}
              rightAnswer={rightAnswer}
              setRightAnswer={setRightAnswer}
            />
          </View>
          <View style={styles.columnContainer}>
            <AnswerComponent
              question={question}
              index={2}
              disabled={disabled}
              setDisabled={setDisabled}
              style={style}
              setStyle={setStyle}
              setIndex={setIndex}
              questionIndex={questionIndex}
              rightAnswer={rightAnswer}
              setRightAnswer={setRightAnswer}
            />
            <AnswerComponent
              question={question}
              index={3}
              disabled={disabled}
              setDisabled={setDisabled}
              style={style}
              setStyle={setStyle}
              setIndex={setIndex}
              questionIndex={questionIndex}
              rightAnswer={rightAnswer}
              setRightAnswer={setRightAnswer}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default QuizScreenComponent;

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  imageContainer: {
    height: "50%",
  },
  textContainer: {
    backgroundColor: "rgba(0,0,0, 0.7)",
    marginHorizontal: 20,
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderRadius: 10,
    height: "100%",
    justifyContent: "space-between",
  },
  headerText: {
    color: "white",
    fontSize: 25,
    fontWeight: "bold",
    alignSelf: "center",
    borderRadius: 10,
  },
});

export default styles;

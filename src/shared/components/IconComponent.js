import React from "react";
import { Icon } from "react-native-elements";

const IconComponent = (props) => {
  const onPress = props.onPress;
  return (
    <Icon
      name="arrow-left"
      type="font-awesome"
      color="#662d00"
      onPress={onPress}
      size={50}
    />
  );
};

export default IconComponent;

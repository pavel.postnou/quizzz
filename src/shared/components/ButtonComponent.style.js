import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  titleStyle: { fontWeight: "bold", fontSize: 15 },
  buttonStyle: {
    borderWidth: 2,
    borderColor: "white",
    borderRadius: 20,
  },
  buttonStyleSignOut: {
    borderWidth: 2,
    borderColor: "white",
    borderRadius: 20,
    backgroundColor: "#ff3636",
  },
  containerStyle: {
    width: "50%",
    marginVertical: 20,
    alignSelf:"center"
  },
  containerStyleSignOut: {
    position: "absolute",
    bottom: 10,
    right: 10,
    width: "30%",
    marginVertical: 20,
  },

  iconContainerStyle: { marginLeft: 10, marginRight: -10 },
});

export default styles;

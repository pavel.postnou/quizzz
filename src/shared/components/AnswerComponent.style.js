import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#874626",
    padding: 3,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: "white",
    alignItems: "center",
    width: "80%",
    height: "90%",
    justifyContent: "center",
  },
  buttonRight: {
    backgroundColor: "#42d435",
    padding: 3,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: "white",
    alignItems: "center",
    width: "80%",
    height: "90%",
    justifyContent: "center",
  },
  buttonWrong: {
    backgroundColor: "#d43535",
    padding: 3,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: "white",
    alignItems: "center",
    width: "80%",
    height: "90%",
    justifyContent: "center",
  },
  buttonDisabled: {
    backgroundColor: "#a6a6a6",
    padding: 3,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: "white",
    alignItems: "center",
    width: "80%",
    height: "90%",
    justifyContent: "center",
  },
  text: {
    color: "white",
    fontWeight: "bold",
  },
  touch: {
    width: "100%",
    alignItems: "center",
    height: "50%",
  },
});

export default styles;

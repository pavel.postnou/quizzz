import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#5c0005",
  },
  headerText: {
    color: "white",
    fontSize: 25,
    fontWeight: "bold",
    alignSelf: "center",
    borderRadius: 10,
  },
  placeText: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15,
    alignSelf: "center",
  },
  playerText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 20,
    alignSelf: "center",
  },
  textButton: {
    color: "white",
    fontWeight: "bold",
  },
});

export default styles;

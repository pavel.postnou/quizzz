import { StyleSheet } from "react-native";
import { width } from "../../../constants/sizeConstants";

const styles = StyleSheet.create({
  scoresContainer: {
    width: width,
    height: "90%",
  },
  textContainer: {
    backgroundColor: "rgba(0,0,0, 0.7)",
    marginHorizontal: 20,
    paddingVertical: 8,
    borderRadius: 10,
    height: "100%",
  },
  headerText: {
    color: "#7583ff",
    fontSize: 25,
    fontWeight: "bold",
    alignSelf: "center",
    borderWidth: 1,
    borderColor: "white",
    padding: 5,
    borderRadius: 10,
    marginVertical: 10,
  },
  playerContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
});

export default styles;

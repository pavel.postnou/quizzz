import { StyleSheet } from "react-native";
import { width } from "../../../constants/sizeConstants";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    buttonsContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      height: "30%",
    },
    imageContainer: {
      height: "50%",
      marginBottom: 10,
    },
    itemContainer: {
      width: width,
      height: "90%",
    },
    textContainer: {
      backgroundColor: "rgba(0,0,0, 0.7)",
      marginHorizontal: 20,
      paddingVertical: 8,
      paddingHorizontal: 8,
      borderRadius: 10,
      height: "100%",
    },
    columnContainer: {
      width: "50%",
      justifyContent: "space-evenly",
      alignItems: "center",
    },
  });
  
  export default styles
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  image: {
    width: "95%",
    height: "100%",
    alignSelf: "center",
    overflow: "hidden",
    borderRadius: 10,
  },
});

export default styles;

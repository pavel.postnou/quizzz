import React from "react";
import { View, useWindowDimensions } from "react-native";
import { useNavigation } from "@react-navigation/core";
import ButtonComponent from "./ButtonComponent";
import ImageComponent from "./ImageComponent";
import styles from "./ChooseScreenComponent.style";
import TextComponent from "./TextComponent";
const ChooseScreenComponent = (props) => {
  const { width: windowWidth } = useWindowDimensions();
  const { quiz, quizIndex, user } = props;
  const navigation = useNavigation();

  return (
    <View style={{ width: windowWidth, height: "90%" }} key={quizIndex}>
      <View style={styles.textContainer}>
        <View style={styles.imageContainer}>
          <ImageComponent uri={quiz[Object.keys(quiz)[0]].title} />
        </View>
        <TextComponent
          style="headerText"
          text={quiz[Object.keys(quiz)[0]].description}
        />
        <ButtonComponent
          title="PLAY"
          iconName="play-circle"
          buttonStyle="buttonStyle"
          containerStyle="containerStyle"
          onPress={() =>
            navigation.navigate("Quiz", {
              quiz: Object.keys(quiz)[0],
              user: user,
            })
          }
        />
      </View>
    </View>
  );
};

export default ChooseScreenComponent;

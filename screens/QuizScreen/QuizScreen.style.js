import { StyleSheet } from "react-native";
import { width, height } from "../../constants/sizeConstants";

const styles = StyleSheet.create({
  imageBackground: {
    width: width,
  },
  quizContainer: {
    width: width,
    height: height,
    justifyContent: "center",
    alignItems: "center",
  },
  iconContainer: {
    position: "absolute",
    bottom: 20,
    left: 20,
  },
  scrollContainer: {
    height: "80%",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default styles;

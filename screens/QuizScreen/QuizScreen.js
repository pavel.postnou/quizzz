import React, { useEffect, useState } from "react";
import { View, ImageBackground } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { db } from "../../firebase";
import { doc, getDoc } from "firebase/firestore";
import QuizScreenComponent from "../../src/shared/components/QuizScreenComponent";
import styles from "./QuizScreen.style";
import IconComponent from "../../src/shared/components/IconComponent";

const QuizScreen = (props) => {
  const { quiz, user } = props.route.params;
  const [questions, setQuestions] = useState([]);
  const [question, setQuestion] = useState(null);
  const [index, setIndex] = useState(0);
  const [rightAnswer, setRightAnswer] = useState(0);
  const navigation = useNavigation();
  const backgroundImage = require("../../assets/main-back.jpg");

  useEffect(async () => {
    const docRef = doc(db, "quizes", quiz);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      setQuestions(docSnap.data().questions);
    } else {
      alert("No such document!");
    }
  }, []);

  useEffect(() => {
    if (questions.length != 0) {
      index <= questions.length - 1
        ? setQuestion(questions[index])
        : navigation.replace("Final", {
            user: user,
            quiz: quiz,
            rightAnswer: rightAnswer,
          });
    }
  }, [questions, index]);

  return (
    <View>
      <ImageBackground
        style={styles.imageBackground}
        source={backgroundImage}
        resizeMode="cover"
      >
        <View style={styles.quizContainer}>
          <View style={styles.iconContainer}>
            <IconComponent
              iconName="arrow-left"
              onPress={() => navigation.goBack()}
            />
          </View>
          <View style={styles.scrollContainer}>
            {question ? (
              <QuizScreenComponent
                question={question}
                questionIndex={index}
                setIndex={setIndex}
                rightAnswer={rightAnswer}
                setRightAnswer={setRightAnswer}
              />
            ) : null}
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default QuizScreen;

import { StyleSheet } from "react-native";
import { width, height } from "../../constants/sizeConstants";

const styles = StyleSheet.create({
  imageBackground: {
    width: width,
  },
  topScreenContainer: {
    width: width,
    height: height,
    justifyContent: "center",
    alignItems: "center",
  },
  imageContainer: {
    position: "absolute",
    top: 20,
  },
  iconContainer: {
    position: "absolute",
    bottom: 20,
    left: 20,
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  scrollContainer: {
    height: "80%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "15%",
  },
  normalDot: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: "silver",
    marginHorizontal: 5,
  },
  indicatorContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default styles;

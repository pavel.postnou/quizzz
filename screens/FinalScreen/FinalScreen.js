import React, { useEffect } from "react";
import { View, ImageBackground, Image, Text } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { db } from "../../firebase";
import { doc, setDoc, updateDoc, getDoc } from "firebase/firestore";
import backgroundImage from "../../constants/backgroundImage";
import styles from "./FinalScreen.style";
import ButtonComponent from "../../src/shared/components/ButtonComponent";
import Communications from "react-native-communications";
import TextComponent from "../../src/shared/components/TextComponent";

const FinalScreen = (props) => {
  const { user, quiz, rightAnswer } = props.route.params;
  const navigation = useNavigation();
  const header = require("../../assets/score.png");

  useEffect(async () => {
    const docRef = doc(db, "topscores", quiz);
    const docSnap = await getDoc(docRef);
    let arr = docSnap.data().places;
    arr.push({ name: user.name, score: rightAnswer });
    arr.sort((a, b) => (b.score > a.score ? 1 : -1));
    await setDoc(docRef, {
      places: arr.slice(0, 8),
    });
    const playerDocRef = doc(db, "users", user.email);
    await updateDoc(playerDocRef, {
      lastScore: rightAnswer,
    });
  }, []);

  const sharing = () => {
    Communications.email(null, null, null, quiz, `my Score is ${rightAnswer}`);
  };

  return (
    <View>
      <ImageBackground
        style={styles.imageBackground}
        source={backgroundImage}
        resizeMode="cover"
      >
        <View style={styles.topScreenContainer}>
          <View style={styles.imageContainer}>
            <Image source={header}></Image>
          </View>
          <TextComponent style="text" text={quiz} />
          <TextComponent style="text" text={user.name} />
          <TextComponent style="text" text={`your score ${rightAnswer}`} />
          <ButtonComponent
            title="SHARE"
            iconName="share-alt"
            buttonStyle="buttonStyle"
            containerStyle="containerStyle"
            onPress={() => sharing()}
          />
          <ButtonComponent
            title="BACK TO MENU"
            iconName="hand-o-left"
            buttonStyle="buttonStyle"
            containerStyle="containerStyle"
            onPress={() => navigation.replace("Menu", { user: user })}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

export default FinalScreen;

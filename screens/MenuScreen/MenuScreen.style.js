import { StyleSheet } from "react-native";
import { width, height } from "../../constants/sizeConstants";

const styles = StyleSheet.create({
  menuContainer: {
    width: width,
    height: height,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: width,
  },
  headerContainer: {
    position: "absolute",
    top: 50,
  },
  textScore:{
    position: "absolute",
    top: 180,
    fontWeight: "bold",
    fontSize: 30,
    color: "#662d00",
  }
});

export default styles
import React, { useEffect, useState } from "react";
import { View, ImageBackground, Image } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { setDoc, doc, getDoc, getDocs, collection } from "firebase/firestore/";
import { db } from "../../firebase";
import { getAuth } from "firebase/auth";
import ButtonComponent from "../../src/shared/components/ButtonComponent";
import TextComponent from "../../src/shared/components/TextComponent";
import backgroundImage from "../../constants/backgroundImage";
import header from "../../constants/header";
import styles from "./MenuScreen.style";

const MenuScreen = (props) => {
  const navigation = useNavigation();
  const user = props.route.params.user;
  const [lastScore, setLastScore] = useState(null);
  const auth = getAuth();

  useEffect(() => {
    (async function databaseConnect() {
      const isUser = await getDoc(doc(db, "users", user.email));
      isUser.data()
        ? setLastScore(isUser.data().lastScore)
        : await setDoc(doc(db, "users", user.email), {
            email: user.email,
            name: user.name,
            lastScore: null,
            position: null,
          });
    })();
  }, []);

  const randomQuiz = async () => {
    const arr = [];
    const querySnapshot = await getDocs(collection(db, "quizes"));
    querySnapshot.forEach((doc) => {
      arr.push({ [doc.id]: doc.data() });
    });
    const random = Math.floor(Math.random() * arr.length);
    navigation.navigate("Quiz", {
      quiz: Object.keys(arr[random])[0],
      user: user,
    });
  };

  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => navigation.navigate("Login"))
      .catch((error) => alert(error.message));
  };

  return (
    <View>
      <ImageBackground
        style={styles.image}
        source={backgroundImage}
        resizeMode="cover"
      >
        <View style={styles.menuContainer}>
          <View style={styles.headerContainer}>
            <Image source={header}></Image>
          </View>
          <TextComponent style="text" text={`Hi`} />
          <TextComponent style="text" text={user.name} />
          <TextComponent style="text" text={`Your last score ${lastScore}`} />
          <ButtonComponent
            title="START"
            iconName="gamepad"
            buttonStyle="buttonStyle"
            containerStyle="containerStyle"
            onPress={() => navigation.navigate("Choose", { user: user })}
          />
          <ButtonComponent
            title="RANDOM QUIZ"
            iconName="random"
            buttonStyle="buttonStyle"
            containerStyle="containerStyle"
            onPress={() => randomQuiz()}
          />
          <ButtonComponent
            title="BEST SCORES"
            iconName="star"
            buttonStyle="buttonStyle"
            containerStyle="containerStyle"
            onPress={() => navigation.navigate("Top")}
          />
          <ButtonComponent
            title="Sign Out"
            iconName="sign-out"
            buttonStyle="buttonStyleSignOut"
            containerStyle="containerStyleSignOut"
            onPress={() => handleSignOut()}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

export default MenuScreen;

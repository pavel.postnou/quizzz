import React, { useRef, useEffect, useState } from "react";
import {
  View,
  ImageBackground,
  ScrollView,
  Animated,
  useWindowDimensions,
  SafeAreaView,
} from "react-native";
import { useNavigation } from "@react-navigation/core";
import { db } from "../../firebase";
import { collection, getDocs } from "firebase/firestore";
import ChooseScreenComponent from "../../src/shared/components/ChooseScreenComponent";
import styles from "./ChooseScreen.style";
import IconComponent from "../../src/shared/components/IconComponent";

const ChooseScreen = (props) => {
  const [quizes, setQuizes] = useState([]);
  const scrollX = useRef(new Animated.Value(0)).current;
  const navigation = useNavigation();
  const backgroundImage = require("../../assets/main-back.jpg");
  const { width: windowWidth } = useWindowDimensions();
  const user = props.route.params.user

  useEffect(async () => {
    const arr = [];
    const querySnapshot = await getDocs(collection(db, "quizes"));
    querySnapshot.forEach((doc) => {
      arr.push({ [doc.id]: doc.data() });
    });
    setQuizes(arr);
  }, []);

  return (
    <View>
      <ImageBackground
        style={styles.imageBackground}
        source={backgroundImage}
        resizeMode="cover"
      >
        <View style={styles.chooseScreenContainer}>
          <View style={styles.iconContainer}>
            <IconComponent onPress={() => navigation.goBack()} />
          </View>
          <SafeAreaView style={styles.container}>
            <View style={styles.scrollContainer}>
              <ScrollView
                horizontal={true}
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                onScroll={Animated.event(
                  [
                    {
                      nativeEvent: {
                        contentOffset: {
                          x: scrollX,
                        },
                      },
                    },
                  ],
                  { useNativeDriver: false }
                )}
                scrollEventThrottle={1}
              >
                {quizes.map((quiz, quizIndex) => (
                  <ChooseScreenComponent key={quizIndex} quiz={quiz} quizIndex={quizIndex} user={user} />
                ))}
              </ScrollView>
              <View style={styles.indicatorContainer}>
                {quizes.map((quiz, quizIndex) => {
                  const width = scrollX.interpolate({
                    inputRange: [
                      windowWidth * (quizIndex - 1),
                      windowWidth * quizIndex,
                      windowWidth * (quizIndex + 1),
                    ],
                    outputRange: [10, 20, 10],
                    extrapolate: "clamp",
                  });
                  return (
                    <Animated.View
                      key={quizIndex}
                      style={[styles.normalDot, { width }]}
                    />
                  );
                })}
              </View>
            </View>
          </SafeAreaView>
        </View>
      </ImageBackground>
    </View>
  );
};

export default ChooseScreen;

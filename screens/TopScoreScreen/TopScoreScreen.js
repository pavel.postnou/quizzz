import React, { useRef, useEffect, useState } from "react";
import {
  View,
  ImageBackground,
  Image,
  ScrollView,
  Animated,
  useWindowDimensions,
  SafeAreaView,
} from "react-native";
import { useNavigation } from "@react-navigation/core";
import { db } from "../../firebase";
import { collection, getDocs } from "firebase/firestore";
import backgroundImage from "../../constants/backgroundImage";
import TopScoresComponent from "../../src/shared/components/TopScoresComponent";
import styles from "./TopScoreScreen.style";
import IconComponent from "../../src/shared/components/IconComponent";

const TopScoreScreen = () => {
  const [scores, setScores] = useState([]);
  const scrollX = useRef(new Animated.Value(0)).current;
  const navigation = useNavigation();
  const header = require("../../assets/top.png");
  const { width: windowWidth } = useWindowDimensions();

  useEffect(async () => {
    const arr = [];
    const querySnapshot = await getDocs(collection(db, "topscores"));
    querySnapshot.forEach((doc) => {
      arr.push({ [doc.id]: doc.data() });
    });
    setScores(arr);
  }, []);

  return (
    <View>
      <ImageBackground
        style={styles.imageBackground}
        source={backgroundImage}
        resizeMode="cover"
      >
        <View style={styles.topScreenContainer}>
          <View style={styles.imageContainer}>
            <Image source={header}></Image>
          </View>
          <View style={styles.iconContainer}>
            <IconComponent onPress={() => navigation.goBack()} />
          </View>
          <SafeAreaView style={styles.container}>
            <View style={styles.scrollContainer}>
              <ScrollView
                horizontal={true}
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                onScroll={Animated.event(
                  [
                    {
                      nativeEvent: {
                        contentOffset: {
                          x: scrollX,
                        },
                      },
                    },
                  ],
                  { useNativeDriver: false }
                )}
                scrollEventThrottle={1}
              >
                {scores.map((table, tableIndex) => (
                  <TopScoresComponent
                    tableIndex={tableIndex}
                    table={table}
                    key={tableIndex}
                  />
                ))}
              </ScrollView>
              <View style={styles.indicatorContainer}>
                {scores.map((table, tableIndex) => {
                  const width = scrollX.interpolate({
                    inputRange: [
                      windowWidth * (tableIndex - 1),
                      windowWidth * tableIndex,
                      windowWidth * (tableIndex + 1),
                    ],
                    outputRange: [10, 20, 10],
                    extrapolate: "clamp",
                  });
                  return (
                    <Animated.View
                      key={tableIndex}
                      style={[styles.normalDot, { width }]}
                    />
                  );
                })}
              </View>
            </View>
          </SafeAreaView>
        </View>
      </ImageBackground>
    </View>
  );
};

export default TopScoreScreen;

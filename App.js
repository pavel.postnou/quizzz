import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginScreen from "./screens/LoginScreen";
import MenuScreen from "./screens/MenuScreen";
import TopScoreScreen from "./screens/TopScoreScreen";
import ChooseScreen from "./screens/ChooseScreen";
import QuizScreen from "./screens/QuizScreen";
import FinalScreen from "./screens/FinalScreen";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="Login"
          component={LoginScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Menu"
          component={MenuScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Top"
          component={TopScoreScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Choose"
          component={ChooseScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Quiz"
          component={QuizScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Final"
          component={FinalScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

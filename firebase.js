// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore/";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDjey6Ao9dIY7PWn2SLvxkCxYtAlnnu02U",
  authDomain: "quizzz-4153a.firebaseapp.com",
  projectId: "quizzz-4153a",
  storageBucket: "quizzz-4153a.appspot.com",
  messagingSenderId: "697962540917",
  appId: "1:697962540917:web:9abfe79ec7ddc747fead3c",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { app, db };
